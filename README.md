# Atividade Pratica



## Questão 3

<p>O código do arquivo calculadora.js é inicializado a partir do comando "node calculadora.js num_1 num_2", no terminal, e exibe na tela o resultado da soma a partir do console.log.</p>
<p>Vale ressaltar que, caso o código não seja executado, pode ser necessário instalar o Node.JS na máquina. No Linux, ele pode ser instalado pelo comando # sudo apt install nodejs.</p>

## Questão 4

<p>Para adicionar somente o arquivo calculadora.js, basta executar o comando "git add calculadora.js". </p>
<p>Segundo a convenção, esse commit deve ser "feat: código da calculadora". </p>
<p>O arquivo do REAMDE.md deve ser "feat: questões 3 e 4 no README.md".</p>


## Questão 5
<p>O novo código agora possui uma arrow function indicando a operação de soma.</p>

## Questao 6
<p>Para executar o código, é necessário colocar, no terminal, o comando de chamada do node, o arquivo, a operação desejada e os dois números que estarão na operação. Ex: "node calculadora.js soma num_1 num_2"</p>
<p>Entre os erros do Joãozinho, destaca-se que nas linhas 2 e 6, o vetor args está pegando o arg zero que indica a operação desejada, e o arg[1], que está pegando o primeiro número, quando na verdade deveria ser arg[1] e arg[2].</p>
<p>Outro erro está no default, onde, ao invés de escrever args[0], estava escrito arg[0].</p>

## Questão 7 
<p>Branch criada a partir do comando "git checkout -b develop main"</p>
<p>Operação de divisão adicionada</p>

## Questão 8
<p>A merge foi feita a partir do merge request do gitlab</p>

## Qeustão 9
<p>O revert foi feito para o commit antes da alteração do código, voltando para a divisão.</p>

## Questão 10
<p>Para executar o novo código, primeiro foi preciso adicionar uma linha para remover o "node calculadora.js" do argv, assim, o valor x seria a posição 0, y, a 2, e o operador a 1. Sendo que este operador pode ser qualquer operador aritmético</p>
<p>O código agora pega todos os operadores para a operação matemática e, ao exibir na tela pelo comando console.log, chama a função evaluate que executa a função nativa eval do javascript, que converte em código uma string.</p>